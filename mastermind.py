###########################

# Mastermind

##########################

import requests as rq
import json

import requests as rq
import json

import sys
import itertools

def read_file(file_name: str) -> list[str]:
    return [word for word in open(file_name).read().strip().split()]

data = read_file(sys.argv[1])

'''

def feedback (word : str) -> int:
    feedback = 0
    distinct_letters = set(letters for letters in word)
    for letter in distinct_letters:
        if letter in guess_word:
            feedback += 1
    return feedback

def message(COMP: str, guess_word: str) -> str:
	guesses = 0
	while feedback(guess_word) != 5:
		guesses += 1
		if COMP == guess_word:
			return 'WIN'
		else:
			return f'You have made {guesses} in total.'
'''
session = rq.Session()

def register() -> str:
     base_url = "https://we6.talentsprint.com/wordle/game/"

    register_url = base_url + "register"
    register_dict = {"mode" : "mastermind" , "name": "Sri Keerthana"}
    register_with = json.dumps(register_dict)

    #session  = rq.Session()
    session.post(register_url, json = register_dict)
    response_json = session.post(register_url, json = register_dict)
    
    game_id = response_json.json()["id"]

    return game_id
  
#returns ID

ID = register()


    creat_url = base_url + "create"

    creat_dict = {"id" : game_id, "overwrite" : True }

    rc = rq.post(creat_url, json = creat_dict)

    return rc.json()

def guess(word: str) -> str:
    
    base_url = "https://we6.talentsprint.com/wordle/game/"

    guess = {"id" : register(), "guess": "great"}

    guess_url = base_url + "guess"

    correct = session.post(guess_url, json = guess)

    return correct.json()
	pass
#gives feedback as 0 1 2 3 4 5 or message if won


data_ = ["hi","hello"]

def guesser():
    impossibles = set()
    possible = set()

    def is_possible(word: str) -> bool:
        return all(letter not in impossibles for letter in word)

    def no_match(word: str) -> set:
        return set(letter for letter in word)

	def generate_permutations(word:str) -> list[str]:
		permutations  = itertools.permutations(word)
		permuted_words = [''.join(perm) for perm in permutations]
		return permuted_words


    for word in data_:

        if is_possible(word):    
            feedback = guess(word)
            if feedback == '5': 
                    for i in generate_permutations(word):
                        if  i == word:
                            return word     
            elif feedback == '0':   
                impossibles = impossibles | no_match(word)
            else:
                for letter in word:
                    if letter in guess(word):
                        possible.add(letter)


      
    return impossibles , possible


print(guesser())
